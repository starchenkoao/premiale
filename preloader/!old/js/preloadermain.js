var App = function() {};
App.prototype =
{
    // Window width and height
    w: 0,
    h: 0,

    // Objects
    container: null,
    bgImg: null,
    catcherImg: null,
    grassImg_2: null,
    grassImg_3: null,
    grassImg_4: null,
    dropImg: null,
    progressContainer: null,
    progress: null,
    progressPercentage: null,

    conatinerName: 'bottle',
    
    // Falling drop position
    dropY: 0,
    dropX: 0,

    init: function()
    {
        this.updateSettings( $(window).width(), $(window).height() );

        this.container = $("#main_container");
        this.bgImg = $("#bg_img");
        this.catcherImg = $("#bottle");
        this.grassImg_2 = $("#grass_02");
        this.grassImg_3 = $("#grass_03");
        this.grassImg_4 = $("#grass_04");
        this.dropImg = $("#drop_img");
        this.progressContainer = $("#progress_container");
        this.progress = $("#progress");
        this.progressPercentage = $("#messages");
    },

    updateSettings: function(_w, _h)
    {
        this.w = _w;
        this.h = _h;
    },

    updateScreenParam: function()
    {
        this.updateSettings( $(window).width(), $(window).height() );
        this.setElementFullSize(this.container, this.w, this.h);
        $prlx.updateSettings(this.w, this.h);

        this.bgImg.height(this.h);
        this.bgImg.width(this.w * 1.3);
        //this.catcherImg.height(this.h*0.6);
        //this.catcherImg.height(this.h*0.6);        
        //this.catcherImg.width(this.w*0.6);
        
        this.grassImg_2.height(this.h*0.2);
        this.grassImg_3.height(this.h*0.25);
        this.grassImg_4.height(this.h*0.12);
        //this.dropImg.height(this.w*0.03);

        if( $prlx.getItemByName("bottle") != 'undefined' )
        {
            $prlx.getItemByName("bottle").updateOffsetY( this.h*0.2 - this.catcherImg.height() );
            $prlx.getItemByName("bottle").updateOffsetX(-this.catcherImg.width()*0.5 );
            $prlx.getItemByName("bottle").updateRotateCoef(0.015, this.w/2);
        }
        if( $prlx.getItemByName("grass_img_2") != 'undefined' )
        {
            $prlx.getItemByName("grass_img_2").updateOffsetY(this.h - this.grassImg_2.height() );
            $prlx.getItemByName("grass_img_2").updatePosX(this.w - this.w*0.1);
        }
        if( $prlx.getItemByName("grass_img_3") != 'undefined' )
        {
            $prlx.getItemByName("grass_img_3").updateOffsetY(this.h - this.grassImg_3.height() );
            $prlx.getItemByName("grass_img_3").updatePosX(this.w - this.w*0.6);
        }
        if( $prlx.getItemByName("grass_img_4") != 'undefined' )
        {
            $prlx.getItemByName("grass_img_4").updateOffsetY(this.h - this.grassImg_4.height() );
            $prlx.getItemByName("grass_img_4").updatePosX(this.w - this.w*0.9);
        }
    },

    setElementFullSize: function(el, _w, _h)
    {
        el.width(_w);
        el.height(_h);
    },

    initParallaxItems: function()
    {
        $prlx.updateSettings(this.w, this.h);

        // Add all parallaxed objects
        $prlx.addItemByName(this.bgImg, "bg_img", 0, 0, -0.25, 0);
        $prlx.addItemByName(this.catcherImg, "bottle", 0, 0, 1, 1);
        $prlx.addItemByName(this.grassImg_2, "grass_img_2", 0, 0, -0.7, 0);
        $prlx.addItemByName(this.grassImg_3, "grass_img_3", 0, 0, -0.45, 0);
        $prlx.addItemByName(this.grassImg_4, "grass_img_4", 0, 0, -0.5, 0);
        $prlx.addItemByName(this.dropImg, "drop_img", 0, 0, 0, 0);
    }
};

var app = new App();
var preloaderLooper;


var curMouseX = 0;
var curMouseY = 0;
var curX = 0;
var curY = 0;
var coef = 3;
var coefInc = coef + 1;

var progressLoader = 0;
var done = 0;

$(document).ready(function() {
    $.preloadImages([
        "img/preloader/drop-1.png", 
        "img/preloader/drop-2.png",
        "img/preloader/bottle-0.png", 
        "img/preloader/bottle-1.png",
        "img/preloader/bottle-2.png",
        "img/preloader/bottle-3.png",
        "img/preloader/bottle-4.png",
        "img/preloader/bottle-5.png",
        "img/preloader/bottle-6.png",
        "img/preloader/bottle-7.png",
        "img/preloader/bottle-8.png",
        "img/preloader/bottle-9.png"
        ], function () {
        initApp();
    });
});

function initApp()
{
    app.init();
    app.initParallaxItems();
    app.updateScreenParam();
   
    loop();
    preloaderLooper = setInterval(loop, 30);

    app.container.css("opacity", "1");
}

$(window).resize(function() {
    app.updateScreenParam();
});

$("body").mousemove(function( event ) {
    curMouseX = event.pageX;
    curMouseY = event.pageY;
});

function loop()
{
    curX = (curX * coef + curMouseX) / coefInc;
    curY = (curY * coef + curMouseY) / coefInc;

    $prlx.loop(curX, curY);

    app.dropY += app.h*0.025;
    if(app.dropY > app.h)
    {
        app.dropY = -50;
        app.dropX = Math.random()*app.w;
        $prlx.getItemByName("drop_img").updateOffsetX( app.dropX );
    }
    
    var yc=$prlx.getItemByName("bottle").offsetY,
    yd=app.dropY;
    var dd = 'kaplya='+yd+'<br>bottle='+yc;
    
    if((app.dropX > curX - app.catcherImg.width()/2 && app.dropX < curX + app.catcherImg.width()/2) &&
       (app.dropY > curY-224 && app.dropY < curY-204) 
    )    
    //if((app.dropY > $prlx.getItemByName("bottle").offsetY) && (app.dropX > curX - app.catcherImg.width()/2 && app.dropX < curX + app.catcherImg.width()/2) )
    {
        done++;
        //if(done > progressLoader/10) done = Math.round(progressLoader/10);
        
        oldname = app.catcherImg.attr('src');        
        newname = 'img/preloader/bottle-'+done+'.png';
        
        if (newname!=oldname && done < 10) {            
            app.catcherImg.attr('src',newname);
         }   
                 
         if (done>9)
         {
            clearInterval(preloaderLooper);
            $('#preloader').hide();
            $('#pass').show();
            $('#introplay').show();
         }
        
        app.dropY = -50;
        app.dropX = Math.random()*app.w;
        $prlx.getItemByName("drop_img").updateOffsetX( app.dropX );
    }
    if( $prlx.getItemByName("drop_img") != null )
    {
        $prlx.getItemByName("drop_img").updateOffsetY( app.dropY );
    }

   // updateInroLoadingStatus();  // Stub
   // app.progressContainer.css("width", progressLoader + "%");
}

// Stub. progressLoader - is current intro loading status in percentage.
function updateInroLoadingStatus (progress)
{    
    progressLoader = progress;
    //if(progressLoader > 100)  progressLoader = 100;
    app.progressPercentage.text(done+' ' + progressLoader + "%");
    //app.progressPercentage.css("left", (progressLoader + 0.3) + "%");
}

// Preload images processing
jQuery.preloadImages = function () {
    if (typeof arguments[arguments.length - 1] == 'function') {
        var callback = arguments[arguments.length - 1];
    } else {
        var callback = false;
    }
    if (typeof arguments[0] == 'object') {
        var images = arguments[0];
        var n = images.length;
    } else {
        var images = arguments;
        var n = images.length - 1;
    }
    var not_loaded = n;
    for (var i = 0; i < n; i++) {
        jQuery(new Image()).attr('src', images[i]).load(function() {
            if (--not_loaded < 1 && typeof callback == 'function') {
                callback();
            }
        });
    }
}
