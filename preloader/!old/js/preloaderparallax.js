// Parallax item object
var PrlxItem = function() {};
PrlxItem.prototype = {
    elem: null,
    posX: 0,
    posY: 0,
    currentPosX: 0,
    currentPosY: 0,
    offsetX: 0,
    offsetY: 0,
    offsetXCoef: 0,
    offsetYCoef: 0,
    rotateCoef: 0,
    rotatePivot: 0,

    init: function(_elem, _posX, _posY, _offsetXCoef, _offsetYCoef)
    {
        this.elem = _elem;
        this.posX = _posX;
        this.posY = _posY;
        this.offsetXCoef = _offsetXCoef;
        this.offsetYCoef = _offsetYCoef;
    },

    updateOffsetX: function(_offsetX)
    {
        this.offsetX = _offsetX;
    },

    updateOffsetY: function(_offsetY)
    {
        this.offsetY = _offsetY;
    },

    updatePosX: function(_posX)
    {
        this.posX = _posX;
    },

    updateRotateCoef: function(_rotateCoef, _rotatePivot)
    {
        this.rotateCoef = _rotateCoef;
        this.rotatePivot = _rotatePivot;
    },

    updatePosition: function(_newPosX, _newPosY)
    {
        this.currentPosX = this.posX + _newPosX * this.offsetXCoef + this.offsetX;
        this.currentPosY = this.posY + _newPosY * this.offsetYCoef + this.offsetY;

        // Update X/Y positions
        this.elem.css( {left: this.currentPosX, top: this.currentPosY} );
        this.elem.css( {
            WebkitTransform: 'rotate(' + ( this.rotateCoef * (this.currentPosX - this.rotatePivot) ) + 'deg)',
            '-ms-transform': 'rotate(' + ( this.rotateCoef * (this.currentPosX - this.rotatePivot) ) + 'deg)',
            '-ms-transform': 'rotate(' + ( this.rotateCoef * (this.currentPosX - this.rotatePivot) ) + 'deg)'
        } );

        // Update rotation
        this.elem.css("transform", 'rotate(' + ( this.rotateCoef * (this.currentPosX - this.rotatePivot) ) + 'deg)');
        this.elem.css("WebkitTransform", 'rotate(' + ( this.rotateCoef * (this.currentPosX - this.rotatePivot) ) + 'deg)');
        this.elem.css("-ms-transform", 'rotate(' + ( this.rotateCoef * (this.currentPosX - this.rotatePivot) ) + 'deg)');
        this.elem.css("-webkit-transform", 'rotate(' + ( this.rotateCoef * (this.currentPosX - this.rotatePivot) ) + 'deg)');
    }
}

// Parallax processing object
var Prlx = function() {};
Prlx.prototype = {
    w: 0,
    h: 0,
    collect: {},
    collectLen: 0,

    updateSettings: function(_w, _h)
    {
        this.w = _w;
        this.h = _h;
    },

    addItemByName: function(_element, _name, _posX, _posY, _offsetXCoef, _offsetYCoef)
    {
        _element.css({  position: "absolute",
            marginLeft: 0,
            marginTop: 0,
            top: _posY,
            left: _posX
        });

        var item = new PrlxItem();
        item.init(_element, _posX, _posY, _offsetXCoef, _offsetYCoef);

        this.collect[_name] = item;
        this.collectLen++;
    },

    getItemByName: function(_name)
    {
        return this.collect[_name];
    },

    loop: function(_x, _y)
    {
        for (var i in this.collect)
        {
            this.collect[i].updatePosition(_x, _y);
        }
    }
}
var $prlx = new Prlx();
