$(window).load(function(){
    $('.bNav').addClass('mHidden');
});
$(document).ready(function () {
    var htmlFontSizeRatio = 67;

    $(window).resize(function () {
        var $windowHeight = $(window).height();

        $(".bContent").css('height', ($windowHeight + 'px'));
        setTimeout(function(){
            $('html').css('fontSize', (($windowHeight/htmlFontSizeRatio).toFixed() + 'px'));
            setTimeout(function(){app.updateScreenParam();},300);
        },300);
    });

    var $windowHeight = $(window).height();
    $('.eNav_btn').click( function(){
        $('.bNav').toggleClass('active');
    });
    $(".bContent").css('height', ($windowHeight + 'px'));
    $('html').css('fontSize', (($windowHeight/htmlFontSizeRatio).toFixed() + 'px'));

    /*
    * parallax
    */
    $('.bParallax').parallax({
        calibrateX: true,
        calibrateY: true,
        invertX: false,
        invertY: false,
        limitX: 10,
        limitY: 10,
        scalarX: 2,
        scalarY: 2,
        frictionX: 0.5,
        frictionY: 0.5
    });
    $('.bNav').addClass('mVisible');
});

