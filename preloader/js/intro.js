var Remote;
var box,
    imgloader = '<img src="img/loader.gif">',
    mobimgloader = '<center><img src="img/mloader.gif"></center>',
    buttonskip = '<div id="bt_nextstage"><button id="skipstep" onclick="nextstage(Remote);">Cледующий шаг</button></div>',
    buttonreload = '<br><button id="reloadaction" onclick="nextstage(Remote);">Начать заново</button>',
    startwithoutmob = '<br><button id="reloadaction" onclick="startintro(false);">Начать без мобильного устройства</button>',
    shakes = 0,    
    rcontrol = false;
    currentstage = 0,    
    //waitsec = 2000,
    orientation = 'vertical',
    orientationchange = false,
    currentmilkclip = 0,
    lastplay = false,    
    swipecatched = false;
    percentLoaded=0,
    imFps = 15,
    imWidth = 800,
    imHeight = 600,
    toImg1 = 198,
    currLoad1 = 0,    
    bottleopen = false,
    skip=false,
    
    link = document.location.hostname+'/r',
    wshost = 's54.com.ua', //websocket server host ip or name
    //wshost = document.location.hostname,
    
    lastvar = 0;


var Client = new RemoteController("ws://"+wshost+":18811");

var loadtext = ["Чтобы начать презентацию отключите автоповорот экрана на своем мобильном и перейдите ссылке:",
    "<p>или отсканируйте этот QR код, используя сканер кодов на вашем устройстве"];
    
//var nextstep;
var messages = [
		'сообщение 1: по экрану мобильного движение вверх',
		'сообщение 2: по экрану мобильного движение вправо',
		'сообщение 3: наклонить устройство',
        'сообщение 4: потрясти устройство',
        'сообщение 5: наклонить устройство', 
            '','','',
	];  
//var rmessages = [
var remoteActions = [
		'',
		'<center>проведите по экрану вправо</center>',
		'<center>наклоняем влево</center>',
        '<center>потрясите устройство влево-вправо</center>',
        '<center>наклоняем устройство</center>'
	];

var preloader = function (e,msg) {    
    var count = parseInt(msg.replace('Image #','').replace(' has been loaded',''));
    //var count = parseInt(msg.match(/\d+$/));
    percentLoaded = Math.round(count*100/toImg1);
    updateInroLoadingStatus(percentLoaded);    
    
    if (percentLoaded == 100) {
  
    }
    else
        $('#messages').html('Loading: '+percentLoaded+'% ');
        //*/
};
$(document).ready(function(){
    //$('#preloader').hide();
    $('#introplay').on('verbose', preloader);
    $('#introplay').jsMovie({
        folder: "img/video/",
        sequence: 'ani_###.jpg',
        from: 1,
        to: toImg1,
        //loadParallel: 5,
        fps: imFps, 
        width: imWidth,
        height: imHeight,
        repeat: false,
        performStop: true,
    });
    
    $("#introplay").jsMovie("addClip","startClip",1,16,0);    
    $("#introplay").jsMovie("addClip","stage2Clip",18,36,0);        
    $("#introplay").jsMovie("addClip","milktoglass",67,169,0);
    $("#introplay").jsMovie("addClip","shake",169,198,0);
    $("#introplay").jsMovie("addClip","stage5Clip",37,66,0);      
    
    $("#introplay").bind("ended",moviestoped);
    
   
});

var moviestoped = function () {
    
    swipecatched = false;    
    $("#introplay").data('myStatus','stoped');
    
    $('#bt_nextstage').show();
    currentstage = 'end'+currentstage;
    nextstage(Remote);
}

var mprint = function(txt) {
    $('#messages').append(txt);
}

// stage 0, stage 1
var swipeHandler = function (e) {
    var direction = e.raw.direction;        
    switch (currentstage) {
    case 0:
        if (direction == 'up' && !swipecatched) {
            swipecatched = true;   
            $('#bt_nextstage').hide();    
            $("#introplay").jsMovie("playClip","startClip",false,false);
            Remote.write("mobileStage(0);");
        }
        break
    case 1:
        if (direction == 'left' && bottleopen) {
            bottleopen = false;            
            $('#introplay').jsMovie("gotoFrame",16);
            Remote.write("mobileStage('bt_close');");
        }
        if (direction == 'right' && !swipecatched) {
            
            if(bottleopen) 
            {
                swipecatched = true;
                $('#bt_nextstage').hide();
                $("#introplay").jsMovie("playClip","stage2Clip",false,false);
                Remote.write("mobileStage(1);");
            } else
            {
                bottleopen = true;
                $('#introplay').jsMovie("gotoFrame",17);
                Remote.write("mobileStage('bt_open');");
            }
        }            
        break;
    default:break;    
    }
}
// stage 2
var orientationHandler = function (e) {
    orientationchange = true;
    orientation = e.raw.orientation;
    if (orientation == 'lefttilt')
    {
        $('#bt_nextstage').hide();
        $("#introplay").jsMovie("playClip",'milktoglass',false,false);
        Remote.write('mobileStage("milkskew");');
    }
    if (orientation == 'vertical') 
        Remote.write('mobileStage("milkvertical");');
}

// stage 3
var shakeHandler = function (e) {
    var acc = e.raw.acceleration;
    acc.x = Math.abs(Math.round(acc.x));
    acc.y = Math.abs(Math.round(acc.y));
    acc.z = Math.abs(Math.round(acc.z));    
    if ((acc.x > 15 || acc.y > 15 || acc.z > 15) && $("#introplay").data('myStatus')=='stoped')
    {
        shakes++;
        $('#bt_nextstage').hide();
        $("#introplay").data('myStatus','go');
        $("#introplay").jsMovie("playClip",'shake',false,false);        
        Remote.write('mobileStage("shake");');
        //mprint('currmilk='+currentmilkclip+'<br>');
    }
}
// stage 4
var milkDrink = function (e) {
    orientationchange = true;
    orientation = e.raw.orientation;
    if (orientation!='vertical')
    {
        $('#bt_nextstage').hide();
        $("#introplay").jsMovie("playClip",'stage5Clip',false,false);
        Remote.write('mobileStage("milkskew");');
    } 
    else
    {
        Remote.write('mobileStage("milkvertical");');
    }
}

var startintro = function (rc) {
    rcontrol = rc || false;
    $("#out").html(messages[currentstage] + buttonskip);
}

var mobilesynchronize = function (e) {
    var status = e.raw.status;
    if (status >= 100)
    {
        Remote.stop('mobsynch');
        Remote.removeEventListener('mobsynch', mobilesynchronize);        
        startintro(true);
    }
}

Client.addEventListener('connect', function (e) {
    Client.registerClient();
});

Client.addEventListener('register', function (e) {
	
	var pass = e.pass[0]    
    var fulllink = 'http://'+link+'/mobile.html#'+pass;
        
    //'<a target="_blank" href="'+fulllink+'"></a>
    $("#pass").html('<p>'+loadtext[0] + '</p><a class=bb>'+link+' </a> и введите код <a class=bb>'+pass+'</a><p>' + loadtext[1] +  '<div id="qr'+pass+'"></div>');
    $("#qr"+pass).qrcode({text: fulllink});

});

Client.addEventListener('remote', function (e) {    
	Remote = e.remote;
	$("#pass").hide();
    var out=$('#out');
    out.show();
    out.html('<img src="img/loader.gif"> <br> Синхронизация с телефоном');
    
    Remote.addEventListener('mobsynch', mobilesynchronize);
    Remote.watch('mobsynch');
    
	Remote.addEventListener('swipe', swipeHandler);	
	Remote.watch('swipe');	    
    //Remote.addEventListener('devicemotion', shakeHandler);    
	//Remote.addEventListener('deviceorientation', angleHandler);	

});
