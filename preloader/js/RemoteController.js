var isIE = function() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');

    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    if (trident > 0) {
        // IE 11 (or newer) => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    // other browser
    return false;        
}
function createNewEvent(eventname)
{   
    if (isIE())
    {
        var newEvent = window.document.createEvent('Event');
        newEvent.initEvent(eventname, true, true);        
        return newEvent;
    }
    else
    {
        var newEvent = new Event(eventname);
        return newEvent;
    }
    
}
var RemoteController = function(host) {

	var Remote = function(uid, pass) {
		var RemoteController;
		
		var RElement = document.createElement("Remote");
		RElement.toString = function() {return "[object Remote]";}	
	
		RElement.uid = uid;
		RElement.pass = pass;
		RElement.eventList = [];
		
		RElement.watch = function(event, target, alias) {
			target = target || 'window';
			alias = alias || event;
			var msg = {
				act: "watch",
				remote: this.uid,
				data : {
					target: target,
					event: event,
					alias: alias
				}
			}
			this.RemoteController.send(msg);
			RElement.eventList[alias] = createNewEvent(alias);
			return alias;
		}

		RElement.stop = function(alias) {
			var msg = {
				act: "stop",
				remote: this.uid,
				data : {
					alias: alias
				}
			}
			this.RemoteController.send(msg);
			delete RElement.eventList[alias];
		}

		RElement.write = function(target, html) {
			var msg = {
				act: "write",
				remote: this.uid,
				data : {
					target: target,
					html: html
				}
			}
			this.RemoteController.send(msg);
		}
		
		return RElement;
	}

	var RCElement = document.createElement("RemoteController");
	RCElement.toString = function() {return "[object RemoteController]";}
	
//----------------------------------------- EVENTS -----------------------------------------------//
	//connect
	var connectEvent = createNewEvent('connect');

	//fallback to onconnect
	var onconnect = null;
	Object.defineProperty(RCElement, "onconnect", {
		set: function(listener) {
			RCElement.removeEventListener('connect', onconnect);
			onconnect = listener;
			RCElement.addEventListener('connect', listener);
		},
		get: function() {return onconnect;}
	});
	
	//register
    var registerEvent = createNewEvent('register');
	//var registerEvent = new Event("register");
    //var registerEvent = document.createEvent('Event');
    //registerEvent.initEvent('register', true, true);
    
	//fallback to onregister
	var onregister = null;
	Object.defineProperty(RCElement, "onregister", {
		set: function(listener) {
			RCElement.removeEventListener('register', onregister);
			onregister = listener;
			RCElement.addEventListener('register', listener);
		},
		get: function() {return onregister;}
	});

	//attach
    var attachEvent = createNewEvent('attach');
	//var attachEvent = new Event("attach");
    //var attachEvent = document.createEvent('Event');
    //attachEvent.initEvent('attach', true, true);
    
	//fallback to onattach
	var onattach = null;
	Object.defineProperty(RCElement, "onattach", {
		set: function(listener) {
			RCElement.removeEventListener('attach', onattach);
			onattach = listener;
			RCElement.addEventListener('attach', listener);
		},
		get: function() {return onattach;}
	});

	//remote
    var remoteEvent = createNewEvent("remote");    
	//var remoteEvent = new Event("remote");
    //var remoteEvent = document.createEvent('Event');
    //remoteEvent.initEvent('remote', true, true);
    
	//fallback to onremote
	var onremote = null;
	Object.defineProperty(RCElement, "onremote", {
		set: function(listener) {
			RCElement.removeEventListener('remote', onremote);
			onremote = listener;
			RCElement.addEventListener('remote', listener);
		},
		get: function() {return onremote;}
	});	
//----------------------------------------- END EVENTS -------------------------------------------------//

//------------------------------------------ PRIVATE ---------------------------------------------------//
	var registeredEvents = {};

	var socket = new WebSocket(host);
	socket.onopen = function(msg){
		RCElement.dispatchEvent(connectEvent);
	};
	socket.onmessage = function(msg){
//		console.log('recived: '+msg.data);
		router(JSON.parse(msg.data))
	};
	socket.onclose = function(msg){};
	
	var register = function(data) { //client side
		RCElement.isRegistered = true;
		RCElement.uid = data.uid;		
		RCElement.passList = [];
		for (var i=0;i<data.pass.length;i++)
			RCElement.passList[data.pass[i]] = null;
		registerEvent.pass = data.pass;
		RCElement.dispatchEvent(registerEvent);
	}

	var attach = function(data) { //remote side
		RCElement.isAttached = true;
		RCElement.uid = data.uid;
		RCElement.dispatchEvent(attachEvent);
	}
	
	var remote = function(data) { //client side
		var remote = new Remote(data.uid, data.pass);
		remote.RemoteController = RCElement;
		if (typeof RCElement.remoteList === 'undefined') {
			RCElement.remoteList = [];
		}
		RCElement.remoteList[data.uid] = remote;
		RCElement.passList[data.pass] = remote;
		remoteEvent.remote = remote;
		RCElement.dispatchEvent(remoteEvent);
	}

	var watch = function(data) { //remote side
		var elem;
		
		if (data.target[0] == '#') {
			elem = document.getElementById(data.target.substr(1));
		} else {
			elem = eval(data.target);
		}
		
		var handler = function(e) {

			var isStandart = false;
		
			var replacement = function (key, value) {
				if (value === e.target || value === window ) {
					return value.id || value.name;
				}
				return value;
			}

			var raw = JSON.stringify(e, replacement);
            //var raw = JSON.stringify(e);
            
			var msg = {
				act: "fire",
				data: {
					uid: RCElement.uid,
					name: data.alias,
					event: null,
					raw: null
				}				
			}
			
			if (isStandart) {
			} else {
				msg.data.raw = raw;
			}
			
			RCElement.send(msg);
		}
		
		elem.addEventListener(data.event, handler);
		
		registeredEvents[data.alias] = {
			target: elem,
			event: data.event,
			handler: handler
		}
	}

	var write = function(data) { //remote side
		var elem;
		
		if (data.target[0] == '#') {
			elem = document.getElementById(data.target.substr(1));
		} else {
			elem = eval(data.target);
		}
		
		elem.innerHTML = data.html;
		
	}
	
	var stop = function(data) { //remote side

		var e = registeredEvents[data.alias];
		e.target.removeEventListener(e.event, e.handler);
		delete registeredEvents[data.alias];
		
	}

	var fire = function(data) { //client side
		var remote = RCElement.remoteList[data.uid];		
		var fireEvent = remote.eventList[data.name];
		if (!fireEvent) {
//			console.log(data);
			return false;
		}
		if (data.event === null ) {
			fireEvent.raw = JSON.parse(data.raw);
		}
		remote.dispatchEvent(fireEvent);
		
	}	
//---------------------------------------------	END PRIVATE -------------------------------------------//

//----------------------------------------------- ROUTER ----------------------------------------------//
	var router = function(msg) {
		switch (msg.act) {
			case 'register'://client side
				register(msg.data);
				break;
			case 'attach'://remote side
				attach(msg.data);
				break;
			case 'remote'://client side
				remote(msg.data);
				break;
			case 'watch'://remote side
				watch(msg.data);
				break;
			case 'fire'://client side
				fire(msg.data);
				break;
			case 'stop'://remote side
				stop(msg.data);
				break;
			case 'write'://remote side
				write(msg.data);
				break;
			default:
				break;
		}
	}
//----------------------------------------------- END ROUTER -----------------------------------------------//
		
//------------------------------------------------- PUBLIC -------------------------------------------------//

	RCElement.send = function(obj) {
		var msg = JSON.stringify(obj);
		socket.send(msg);
//		console.log('sent: '+msg);
	}

	RCElement.close = function() {
		socket.close();
	}

	RCElement.registerClient = function(secure, limit) {
		secure = (typeof secure === 'undefined') ? true : secure;
		limit = (typeof limit === 'undefined') ? 1 : limit;
	
		var msg = {
			"act": "register",
			"data" : {
				"secure": secure,
				"limit": limit
			}
		}
		this.send(msg);
	}
		
	RCElement.attachRemote = function(pass) {
		var msg = {
			"act": "attach",
			"data" : {
				"pass": pass
			}
		}
		this.send(msg);
	}
		
	return RCElement;
//---------------------------------------------- END PUBLIC -------------------------------------------------//
}