var wshost = 's54.com.ua',
    prevstage = 0;

var preloader = function (e,msg) {
    var count = parseInt(msg.replace('Image #','').replace(' has been loaded',''));    
    precentLoaded = Math.round((count)*100/108);
    if (precentLoaded >= 100) {
        $('#arrow').show();
        $('#arrow').html('&#8593;');
    }   
    else
        $('#arrow').html(precentLoaded+'% ');
    
    var synch = new Event('mobsynch');
    synch['status'] = precentLoaded;
    window.dispatchEvent(synch);    
}

var moviestoped = function () {
    $("#maindiv").data('myStatus','stoped');
    mobileStage('end'+prevstage);
}


var mobileStage = function (currentstage) {
    switch (currentstage)
    {
        case 0:
            prevstage = 0;
            $('#maindiv').jsMovie("option","playBackwards",false);
            $("#maindiv").jsMovie("playClip","Clip1",false,false);
            $("#arrow").animate({opacity: 0}, 1000);
            break;
        case 'end0':
            $("#arrow").css({opacity:1}).html('&#8594');
            break;
        case 'bt_open': $("#maindiv").jsMovie("gotoFrame",17); break;
        case 'bt_close': $("#maindiv").jsMovie("gotoFrame",16); break;
        case 1: 
            prevstage = 1;
            $("#maindiv").jsMovie("playClip","Clip2",false,false);
            $("#arrow").animate({opacity: 0}, 1000);
            break;    
        case 'end1':   
            $("#arrow").hide();
            $("#maindiv").jsMovie("playClip","Clip3",true,false);            
            break;    
        case 'milkskew':
            prevstage = 'milkskew';
            $("#maindiv").jsMovie("playClip","Clip4",false,false);
            break;
        case 'endmilkskew': 
            $("#maindiv").jsMovie("playClip","Clip5",true,false);            
            break;
        case 'milkvertical': 
            prevstage = 'milkvertical';
            var frame = $("#maindiv").data("currentFrame").data('frame');
            $('#maindiv').jsMovie("option","playBackwards",true);
            if (frame <56 || frame > 74) //checkpoint
            {
                $("#maindiv").jsMovie("playClip","Clip4",false,false)
            }  
            break;
        case 'endmilkvertical': 
            $('#maindiv').jsMovie("option","playBackwards",false);
            mobileStage('end1');
            break;
        case 'shake':
            prevstage ='end1';
            $("#maindiv").jsMovie("playClip","Clip6",false,false);
        case 'selectproduct':
            $('#carusel').html();
            for (var i=1;i<6;i++)
                $('#carusel').append('<div class="jsMovieFrame" style="display: block; width: 320px; height: 568px; background-image: url(img/bottles/bottle-'+i+'.png); background-repeat: no-repeat no-repeat;"></div>');
            
            $(".owl-carousel").owlCarousel({
				singleItem:true
			});
            break;
        case 'gotoStart':
            prevstage=0;
            $("#maindiv").show();
            $("#maindiv").jsMovie("stop");
            $("#maindiv").jsMovie("gotoFrame",1);
            $('#arrow').css({opacity:1}).html('&#8593;').show();
            break;
        default:break;
    }
    
}

$(document).ready(function(){

    $(".owl-carousel").owlCarousel({
        singleItem:true
    });

    var Remote = new RemoteController("ws://"+wshost+":18811");
    Remote.addEventListener('connect', function (e) {
        if(window.location.hash) Remote.attachRemote(window.location.hash.replace('#', ''));    
    });
    
    var orientation = false;
    if (window.DeviceOrientationEvent) {
        orientation = true;       
    } 

$('#maindiv').on('verbose', preloader);
    
$('#maindiv').jsMovie({
    sequence: 'a1mob_###.jpg',
    from: 1,
    to: 108,
    fps: 15,
    folder: "../img/videomobile/",
    width: 320,
    height: 568,    
    repeat: false,
    performStop: true,
    //playOnLoad: true,
});



$("#maindiv").jsMovie("addClip","Clip1",1,16,0); //bottle up
$("#maindiv").jsMovie("addClip","Clip2",16,37,0); // cup out
$("#maindiv").jsMovie("addClip","Clip3",37,55,0); // milk
$("#maindiv").jsMovie("addClip","Clip4",56,74,0); // milk slope
$("#maindiv").jsMovie("addClip","Clip5",75,93,0); // milk sloped
$("#maindiv").jsMovie("addClip","Clip6",94,108,0); // milk shake

$("#maindiv").bind("loaded",function () {$("#maindiv").show();});
$("#maindiv").bind("ended",moviestoped);

    var swipe = new Event("swipe");
    var ori = new Event("chngorientation");
    var el=document.getElementById('maindiv');   

    swipedetect(el, function(swipedir){        
        if (swipedir != 'none')
        {    
            swipe['direction']=swipedir;            
            window.dispatchEvent(swipe);
        }
    });

    var prevtime1=0,    
    timedelay=100, // время задержки реагирования на датчик в мс
    lastorientation = // предыдущее стостояние положения на один шаг назад
    prevorientation = 'vertical', // предыдущее стостояние положения после смены
    orientation = 'vertical', // текущее стостояние положения
    changediff = 5, // шаг изменения положения в градусах    
    prev_b = 0, //beta
    prev_g = 0, //gamma
    prev_x = 0,
    prev_y = 0;
    count=0;
    
    
    if(orientation) {
        window.addEventListener('deviceorientation', function(event) {
                var timenow =  new Date();
                if ((timenow.getTime() - prevtime1) < timedelay) return;
                prevtime1 = timenow.getTime();
                if (Math.abs(event.beta - prev_b) > changediff || Math.abs(event.gamma - prev_g) > changediff)
                {                                    
                    prev_b = event.beta;
                    prev_g = event.gamma;
                    if (event.beta > 50 && event.gamma < 40 && event.gamma > -40 && orientation != 'vertical')
                    {
                        prevorientation = orientation;
                        orientation = 'vertical';
                    }
                    if (event.beta < 20 && event.gamma > 50 && orientation != 'righttilt')
                    {
                        prevorientation = orientation;
                        orientation = 'righttilt';
                    }
                    if (event.beta < 20 && event.gamma < -50 && orientation != 'lefttilt')
                    {
                        prevorientation = orientation;
                        orientation = 'lefttilt';
                    }                    
                    if (lastorientation != orientation) 
                    {   
                        count++;
                        ori['prev']=prevorientation; 
                        ori['orientation']=orientation;
                        lastorientation = orientation;
                        window.dispatchEvent(ori);
                    }    
                }
            }, false);
    }
});