$(window).load(function(){
    $('.bNav').addClass('mHidden');
});
$(document).ready(function () {
    var htmlFontSizeRatio = 67;

    $(window).resize(function () {
        var $windowHeight = $(window).height();
//        $(".bContent").css('height', ($windowHeight + 'px'));
        setTimeout(function(){
            $('html').css('fontSize', (($windowHeight/htmlFontSizeRatio).toFixed() + 'px'));
            setTimeout(function(){app.updateScreenParam();},300);
        },100);

    });

    var $windowHeight = $(window).height();

    $('html').css('fontSize', (($windowHeight/htmlFontSizeRatio).toFixed() + 'px'));

    $('.eNav_btn').click( function(){
        $('.bNav').toggleClass('active');
    });
    $('.bNav').addClass('mVisible');

    $(".bSubNavWrap").css('height', ($windowHeight + 'px'));



    /*
    * parallax
    */
    $('.bParallax').parallax({
        calibrateX: true,
        calibrateY: false,
        invertX: false,
        invertY: false,
        limitX: 100,
        limitY: 0,
        scalarX: 5,
        scalarY: 0,
        frictionX: 0.5,
        frictionY: 0.5
    });

    /*
     * news page grid
     */
    var $newsContainer = $('.eNewsItemWrap_inner');
    $newsContainer.isotope({
        // disable scale transform transition when hiding
        hiddenStyle: {
            opacity: 0
        },
        visibleStyle: {
            opacity: 1
        },
        transitionDuration: 0,
        layoutMode: 'masonry',
        sortBy: 'original-order',
        itemSelector: '.bNewsItem'
    });

    // isotope relayout mac os fix
    $(window).load(function () {
        setTimeout(function () {
            $newsContainer.isotope('reLayout');
        }, 200);
    });
    $(window).resize(function () {
        setTimeout(function () {
            $newsContainer.isotope('reLayout');
        }, 200);
    });


});

