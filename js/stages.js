function nextstage(Remote) {
 
    var out=$('#out');
    // stage 0 - bottle out
    // stage 1 - bottle cup remove
    // stage 2 - milk to glass
    // stage 3 - shake glass
    // stage 4 - drink milk 
        
    
    switch(currentstage) {
        case 0: 
            var e={raw: {direction: 'up'}};
            swipeHandler(e);            
            break;
            
        case 'end0':  currentstage=1; break;
        
        case 1: 
            var e={raw: {direction: 'right'}};
            swipeHandler(e);            
            break;
            
        case 'end1':  
            currentstage=2; 
            Remote.stop('swipe');
            $('#introplay').jsMovie("gotoFrame",67);
            Remote.addEventListener('chngorientation', orientationHandler);
            Remote.watch('chngorientation');        
            break;
            
        case 2:
            var e={raw: {orientation: 'lefttilt'}};
            orientationHandler(e);
            break;
            
        case 'end2':
            currentstage=3;
            Remote.stop('chngorientation');
            Remote.removeEventListener('chngorientation', orientationHandler);
            Remote.addEventListener('devicemotion', shakeHandler);
            Remote.watch('devicemotion');
            break;
            
        case 3:
            var e={raw: {acceleration: {x:20,y:20,z:20}}};
            shakeHandler(e);
            break;
            
        case 'end3':
            if (shakes > 1) 
            {  
                Remote.stop('devicemotion'); 
                currentstage=4;
                $('#introplay').jsMovie("gotoFrame",37);
                Remote.removeEventListener('devicemotion', shakeHandler);            
                Remote.addEventListener('chngorientation', milkDrink);
                Remote.watch('chngorientation');
            } else
                currentstage=3;
            break;
        case 4: 
            var e={raw: {orientation: 'lefttilt'}};
            milkDrink(e);
            break;        
            
        case 'end4':
            //currentstage=5;
            Remote.stop('chngorientation');
            setTimeout(function () {
                $('#introplay').jsMovie("option","playBackwards",true);                
                $('#introplay').jsMovie("playClip",'stage5Clip',false,false);
                Remote.write('mobileStage("milkvertical");');
                },1500);
            break;
        case 'endend4':        
            currentstage='reload';
             setTimeout(function () {
                $('#introplay').hide();
                $('#endmessage').show();
                $('#out').html(messages[currentstage] + buttonreload);
                Remote.write('$("#maindiv").hide();mobileStage("selectproduct");');
                },2000);
            break;
        
        case 'reload':
            currentstage = 0;
            bottleopen = false;
            Remote.watch('swipe');
            $('#endmessage').hide();
            $('#introplay').show();
            $('#introplay').jsMovie("gotoFrame",1);
            $('#introplay').jsMovie("option","playBackwards",false);
            Remote.write("mobileStage('gotoStart');");
            break;      
        default:break;
    };
    $('#out').html(messages[currentstage] + buttonskip);
        //Remote.write('#hint', rmessages[currentstage]);
 }